if OBJECT_ID('validanaly') is null
create table validanaly (
   ymd                  varchar(10)          not null,
   organno              varchar(10)          not null,
   smatno               varchar(15)          not null,
   batchno              varchar(30)          not null,
   idno                 varchar(12)          not null,
   shelfno              varchar(20)          not null,
   validday             int                  null,
   validdesc            varchar(50)          null,
   stockqty             numeric(16,4)        null,
   saleqty              numeric(16,4)        null,
   drqty                numeric(16,4)        null,
   dcqty                numeric(16,4)        null,
   syqty                numeric(16,4)        null,
   tmstamp              timestamp            not null,
   guid                 uniqueidentifier     null default newid(),
   constraint PK_VALIDANALY primary key (ymd, organno, smatno, batchno, idno, shelfno)
)
go
if object_id('pro_效期分析') is not null drop proc pro_效期分析
go
create proc pro_效期分析
as
begin

--产生当天效期产品动销明细
if object_id('tempdb..#lsb') is not null drop table #lsb 
select   a.organno,a.smatno,a.batchno,a.idno,a.shelfno,
         cast(0 as numeric(16,4)) stockqty,
         DATEDIFF(day, getdate(), b.validdate) as days,
         cast('' as varchar(50)) daydesc,
         CAST(0 as numeric(16,4)) as saleqty,
         CAST(0 as numeric(16,4)) as drqty,
         CAST(0 as numeric(16,4)) as dcqty,
         CAST(0 as numeric(16,4)) as syqty
into     #lsb         
from     st_stock_d a join st_stock_l b on a.batchno = b.batchno and a.idno = b.idno and a.smatno = b.smatno
where    a.stockqty > 0 and DATEDIFF(day, getdate(), b.validdate) <= 270 and ISDATE(b.validdate) = 1 and  validdate > '2000/01/01'
union
select a.organno,b.smatno,b.batchno,b.idno,b.shelfno,
        cast(0 as numeric(16,4)) stockqty,
         DATEDIFF(day, getdate(), max(b.validdate)) as days,
         cast('' as varchar(50)) daydesc,
         CAST(0 as numeric(16,4)) as saleqty,
         CAST(0 as numeric(16,4)) as drqty,
         CAST(0 as numeric(16,4)) as dcqty,
         CAST(0 as numeric(16,4)) as syqty
from    ls_sale_m a join ls_sale_d b on a.saleno = b.saleno
where   CONVERT(varchar(10),a.accountdate,112) = CONVERT(varchar(10),GETDATE(),112) and
        DATEDIFF(day, getdate(), b.validdate) <= 270 and ISDATE(b.validdate) = 1 and validdate > '2000/01/01'
group by a.organno,b.smatno,b.batchno,b.idno,b.shelfno 
union
select  a.srcorganno as organno,b.smatno,b.batchno,b.idno,b.srcshelfno as shelfno,
        cast(0 as numeric(16,4)) stockqty,
         DATEDIFF(day, getdate(), max(b.validdate)) as days,
         cast('' as varchar(50)) daydesc,
         CAST(0 as numeric(16,4)) as saleqty,
         CAST(0 as numeric(16,4)) as drqty,
         CAST(0 as numeric(16,4)) as dcqty,
         CAST(0 as numeric(16,4)) as syqty
from    ps_move_m  a join ps_move_d b on a.moveno = b.moveno
where   CONVERT(varchar(10),a.checkdate,112) = CONVERT(varchar(10),GETDATE(),112) and a.status = 1 and a.BILLNO  = 'RIT' and 
	    a.remark like '店间调拨申请单%' AND
	     DATEDIFF(day, getdate(), b.validdate) <= 270 and ISDATE(b.validdate) = 1 and validdate > '2000/01/01'
group by a.srcorganno,b.smatno,b.batchno,b.idno,b.srcshelfno
UNION
select  a.objorganno as organno,b.smatno,b.batchno,b.idno,b.objshelfno as shelfno,
        cast(0 as numeric(16,4)) stockqty,
         DATEDIFF(day, getdate(), max(b.validdate)) as days,
         cast('' as varchar(50)) daydesc,
         CAST(0 as numeric(16,4)) as saleqty,
         CAST(0 as numeric(16,4)) as drqty,
         CAST(0 as numeric(16,4)) as dcqty,
         CAST(0 as numeric(16,4)) as syqty
from    ps_move_m  a join ps_move_d b on a.moveno = b.moveno
where   CONVERT(varchar(10),a.checkdate,112) = CONVERT(varchar(10),GETDATE(),112) and a.status = 1 and a.BILLNO  = 'DIT' and 
	    a.remark like '店间调拨申请单%' AND
	     DATEDIFF(day, getdate(), b.validdate) <= 270 and ISDATE(b.validdate) = 1 and validdate > '2000/01/01'
group by a.objorganno,b.smatno,b.batchno,b.idno,b.objshelfno 
union
select a.organno as organno,b.smatno,b.batchno,b.idno,b.shelfno as shelfno,
       cast(0 as numeric(16,4)) stockqty,
         DATEDIFF(day, getdate(), max(b.validdate)) as days,
         cast('' as varchar(50)) daydesc,
         CAST(0 as numeric(16,4)) as saleqty,
         CAST(0 as numeric(16,4)) as drqty,
         CAST(0 as numeric(16,4)) as dcqty,
         CAST(0 as numeric(16,4)) as syqty
from    st_income_m  a join st_income_d b on a.INCOMENO = b.INCOMENO
where   CONVERT(varchar(10),a.checkdate,112) = CONVERT(varchar(10),GETDATE(),112) and a.status = 1 and a.BILLNO  = 'INO'  and
         DATEDIFF(day, getdate(), b.validdate) <= 270 and ISDATE(b.validdate) = 1 and validdate > '2000/01/01'
group by a.organno,b.smatno,b.batchno,b.idno,b.shelfno
UNION
select  a.organno as organno,b.smatno,b.batchno,b.idno,b.shelfno as shelfno,
        cast(0 as numeric(16,4)) stockqty,
         DATEDIFF(day, getdate(), max(b.validdate)) as days,
         cast('' as varchar(50)) daydesc,
         CAST(0 as numeric(16,4)) as saleqty,
         CAST(0 as numeric(16,4)) as drqty,
         CAST(0 as numeric(16,4)) as dcqty,
         CAST(0 as numeric(16,4)) as syqty
from    pf_tradsale_m  a join pf_tradsale_d b on a.tradno = b.tradno
where   CONVERT(varchar(10),a.checkdate,112) = CONVERT(varchar(10),GETDATE(),112) and a.status = 1 and a.BILLNO  in ('TRA','TRB') AND
         DATEDIFF(day, getdate(), b.validdate) <= 270 and ISDATE(b.validdate) = 1 and validdate > '2000/01/01'
group by a.organno,b.smatno,b.batchno,b.idno,b.shelfno       


--计算分段
update #lsb
set    daydesc = case when days > 180 and days <=270 then 'E(181-270天)'
                      when days > 90  and days <=180 then 'D(90-180天)'
                      when days > 60  and days <=90  then 'C(60-90天)'
                      when days > 30  and days <=60  then 'B(30-60天)'
                      when days <=30  then 'A(30天以下)'
                      else null
                  end 
--库存
update  a
set     a.stockqty = ISNULL(b.stockqty,0)
from    #lsb a join st_stock_d b on a.organno = b.organno and a.batchno = b.batchno and a.smatno = b.smatno and a.idno = b.idno and a.shelfno = b.shelfno                     
--销售
update  a
set     a.saleqty = ISNULL(b.qty,0)
from    #lsb a join (select a.organno,b.smatno,b.batchno,b.idno,b.shelfno,sum(qty/b.minnum) as qty
					 from    ls_sale_m a join ls_sale_d b on a.saleno = b.saleno
					 where   CONVERT(varchar(10),a.accountdate,112) = CONVERT(varchar(10),GETDATE(),112)
					 group by a.organno,b.smatno,b.batchno,b.idno,b.shelfno) b on a.organno = b.organno and a.batchno = b.batchno and a.smatno = b.smatno and a.idno = b.idno and a.shelfno = b.shelfno

--调拨调出
update  a
set     a.dcqty = ISNULL(b.qty,0)
from    #lsb a join (select a.srcorganno as organno,b.smatno,b.batchno,b.idno,b.srcshelfno as shelfno,sum(stockqty) as qty
					 from    ps_move_m  a join ps_move_d b on a.moveno = b.moveno
					 where   CONVERT(varchar(10),a.checkdate,112) = CONVERT(varchar(10),GETDATE(),112) and a.status = 1 and a.BILLNO  = 'RIT' and 
							a.remark like '店间调拨申请单%'
					 group by a.srcorganno,b.smatno,b.batchno,b.idno,b.srcshelfno
					 union all
					 select a.organno as organno,b.smatno,b.batchno,b.idno,b.shelfno as shelfno,sum(stockqty) as qty
					 from    pf_tradsale_m  a join pf_tradsale_d b on a.tradno = b.tradno
					 where   CONVERT(varchar(10),a.checkdate,112) = CONVERT(varchar(10),GETDATE(),112) and a.status = 1 and a.BILLNO  = 'TRA' 
					 group by a.organno,b.smatno,b.batchno,b.idno,b.shelfno) b on a.organno = b.organno and a.batchno = b.batchno and a.smatno = b.smatno and a.idno = b.idno and a.shelfno = b.shelfno


--调拨调入
update  a
set     a.drqty = ISNULL(b.qty,0)
from    #lsb a join (select a.objorganno as organno,b.smatno,b.batchno,b.idno,b.objshelfno as shelfno,sum(stockqty) as qty
					 from    ps_move_m  a join ps_move_d b on a.moveno = b.moveno
					 where   CONVERT(varchar(10),a.checkdate,112) = CONVERT(varchar(10),GETDATE(),112) and a.status = 1 and a.BILLNO  = 'DIT' and 
							 a.remark like '店间调拨申请单%'
					 group by a.objorganno,b.smatno,b.batchno,b.idno,b.objshelfno
					 UNION ALL
					 select a.organno as organno,b.smatno,b.batchno,b.idno,b.shelfno as shelfno,sum(stockqty) as qty
					 from    pf_tradsale_m  a join pf_tradsale_d b on a.tradno = b.tradno
					 where   CONVERT(varchar(10),a.checkdate,112) = CONVERT(varchar(10),GETDATE(),112) and a.status = 1 and a.BILLNO  = 'TRB' 
					 group by a.organno,b.smatno,b.batchno,b.idno,b.shelfno) b on a.organno = b.organno and a.batchno = b.batchno and a.smatno = b.smatno and a.idno = b.idno and a.shelfno = b.shelfno

--损益
update  a
set     a.syqty = ISNULL(b.qty,0)
from    #lsb a join (select a.organno as organno,b.smatno,b.batchno,b.idno,b.shelfno as shelfno,sum(stockqtyn - stockqtyo) as qty
					 from    st_income_m  a join st_income_d b on a.INCOMENO = b.INCOMENO
					 where   CONVERT(varchar(10),a.checkdate,112) = CONVERT(varchar(10),GETDATE(),112) and a.status = 1 and a.BILLNO  = 'INO' 
					 group by a.organno,b.smatno,b.batchno,b.idno,b.shelfno
					 ) b on a.organno = b.organno and a.batchno = b.batchno and a.smatno = b.smatno and a.idno = b.idno and a.shelfno = b.shelfno


delete from validanaly
where  ymd = CONVERT(varchar(10),GETDATE(),112)

insert into validanaly(ymd,organno,smatno,batchno,idno,shelfno,validday,validdesc,stockqty,saleqty,drqty,dcqty,syqty) 
select   CONVERT(varchar(10),GETDATE(),112),organno,smatno,batchno,idno,shelfno,days,daydesc,stockqty,saleqty,drqty,dcqty,syqty
from     #lsb


end
go
if not exists (select 1 from sy_report where ereport="VALIDANALY")
begin
 INSERT sy_report(ereport,creport,refrom,rewhere,rehaving,reorder,memo,menu_id,sqlproc,updatetable,ifdistinct,ftext,fwhere,created_by,created_date,mod_by,mod_date,guid,report_exp,sqlopen,clreport,clwhere) values("VALIDANALY","效期动销分析","validanaly a join st_stock_l b on a.smatno = b.smatno and a.batchno = b.batchno and a.idno = b.idno                    join ba_smat  c on a.smatno = c.smatno",null,null,null,null,0,null,null,0,null,null,"ADMIN","2019-12-29 09:55:18.827","669","2019-12-30 06:47:24.153","C7B55BC9-91ED-4481-AB9D-36E46E375026",null,null,null,null)
 INSERT sy_report_col(ereport,ecolname,ccolname,exp,orderby,width,coltype,colalign,col_flg,col_visible,col_seq,col_key,col_color,colcass,sumcmd,created_by,created_date,mod_by,mod_date,selsql,guid) values("VALIDANALY","batchno","批次","a.batchno+'-'+a.idno",45,800,null,"0","N","Y","N","N","""67108864~tif(mod(getrow(),2)=0,rgb(223,234,207),rgb(255,255,255))""","char",null,"ADMIN","2019-12-29 09:55:19.680","669","2019-12-30 06:47:24.177",null,"37200689-84E2-46B0-9C3D-8BA2AC246F3A")
 INSERT sy_report_col(ereport,ecolname,ccolname,exp,orderby,width,coltype,colalign,col_flg,col_visible,col_seq,col_key,col_color,colcass,sumcmd,created_by,created_date,mod_by,mod_date,selsql,guid) values("VALIDANALY","dcamt","调出进价金额","sum(a.dcqty*b.incomprice)",110,300,"###0.00","1","Y","Y","N","N","""67108864~tif(mod(getrow(),2)=0,rgb(223,234,207),rgb(255,255,255))""",null,"SUM(dcamt FOR ALL)","ADMIN","2019-12-29 10:12:20.297","669","2019-12-30 06:47:24.203",null,"A9E854BC-E24A-4C7F-A249-A68CC6A9AED0")
 INSERT sy_report_col(ereport,ecolname,ccolname,exp,orderby,width,coltype,colalign,col_flg,col_visible,col_seq,col_key,col_color,colcass,sumcmd,created_by,created_date,mod_by,mod_date,selsql,guid) values("VALIDANALY","dcqty","当日调出数量","sum(a.dcqty)",105,300,"###0.00","1","Y","Y","N","N","""67108864~tif(mod(getrow(),2)=0,rgb(223,234,207),rgb(255,255,255))""",null,null,"ADMIN","2019-12-29 10:05:12.203","669","2019-12-30 06:47:24.200",null,"15969836-61BA-4380-91A7-9A903218D1D7")
 INSERT sy_report_col(ereport,ecolname,ccolname,exp,orderby,width,coltype,colalign,col_flg,col_visible,col_seq,col_key,col_color,colcass,sumcmd,created_by,created_date,mod_by,mod_date,selsql,guid) values("VALIDANALY","dramt","调入进价金额","sum(a.drqty*b.incomprice)",100,300,"###0.00","1","Y","Y","N","N","""67108864~tif(mod(getrow(),2)=0,rgb(223,234,207),rgb(255,255,255))""",null,"SUM(dramt FOR ALL)","ADMIN","2019-12-29 10:12:20.137","669","2019-12-30 06:47:24.200",null,"7CD44882-2EB0-4FDF-8AB8-A830616A50D6")
 INSERT sy_report_col(ereport,ecolname,ccolname,exp,orderby,width,coltype,colalign,col_flg,col_visible,col_seq,col_key,col_color,colcass,sumcmd,created_by,created_date,mod_by,mod_date,selsql,guid) values("VALIDANALY","drqty","当日调入数量","sum(a.drqty)",95,300,"###0.00","1","Y","Y","N","N","""67108864~tif(mod(getrow(),2)=0,rgb(223,234,207),rgb(255,255,255))""",null,null,"ADMIN","2019-12-29 10:05:12.040","669","2019-12-30 06:47:24.197",null,"DC329BEA-1F9D-48B1-ADB3-CBEFB8783B8F")
 INSERT sy_report_col(ereport,ecolname,ccolname,exp,orderby,width,coltype,colalign,col_flg,col_visible,col_seq,col_key,col_color,colcass,sumcmd,created_by,created_date,mod_by,mod_date,selsql,guid) values("VALIDANALY","incomprice","进价","max(b.incomprice)",70,300,null,"1","Y"," [批次-batchno]","N","N","""67108864~tif(mod(getrow(),2)=0,rgb(223,234,207),rgb(255,255,255))""","deci",null,"ADMIN","2019-12-29 10:05:11.600","669","2019-12-30 06:47:24.187",null,"0B3C4955-372F-4DE3-8FF1-E3EE8C824568")
 INSERT sy_report_col(ereport,ecolname,ccolname,exp,orderby,width,coltype,colalign,col_flg,col_visible,col_seq,col_key,col_color,colcass,sumcmd,created_by,created_date,mod_by,mod_date,selsql,guid) values("VALIDANALY","lotno","批号","b.lotno",65,350,null,"0","N","Y","N","N","""67108864~tif(mod(getrow(),2)=0,rgb(223,234,207),rgb(255,255,255))""","char",null,"ADMIN","2019-12-29 10:05:11.443","669","2019-12-30 06:47:24.183",null,"57FF3C72-5D8B-4A86-9D1C-5C0F8DAA751C")
 INSERT sy_report_col(ereport,ecolname,ccolname,exp,orderby,width,coltype,colalign,col_flg,col_visible,col_seq,col_key,col_color,colcass,sumcmd,created_by,created_date,mod_by,mod_date,selsql,guid) values("VALIDANALY","organno","机构","a.organno",10,600,null,"0","N","Y","N","N","""67108864~tif(mod(getrow(),2)=0,rgb(223,234,207),rgb(255,255,255))""","char",null,"ADMIN","2019-12-29 09:55:19.383","669","2019-12-30 06:47:24.160","dddw_organno#organname*organno","A043FA8A-2151-48E2-B17A-46930C9B1606")
 INSERT sy_report_col(ereport,ecolname,ccolname,exp,orderby,width,coltype,colalign,col_flg,col_visible,col_seq,col_key,col_color,colcass,sumcmd,created_by,created_date,mod_by,mod_date,selsql,guid) values("VALIDANALY","producer","生产企业","max(c.producer)",25,600,null,"0","Y"," [商品-smatno]","N","N","""67108864~tif(mod(getrow(),2)=0,rgb(223,234,207),rgb(255,255,255))""","char",null,"ADMIN","2019-12-29 10:29:02.940","669","2019-12-30 06:47:24.167",null,"894338B7-6F11-489F-90DD-CAA500A5FE6B")
 INSERT sy_report_col(ereport,ecolname,ccolname,exp,orderby,width,coltype,colalign,col_flg,col_visible,col_seq,col_key,col_color,colcass,sumcmd,created_by,created_date,mod_by,mod_date,selsql,guid) values("VALIDANALY","saleamt","销售进价金额","sum(a.saleqty*b.incomprice)",90,300,"###0.00","1","Y","Y","N","N","""67108864~tif(mod(getrow(),2)=0,rgb(223,234,207),rgb(255,255,255))""",null,"SUM(saleamt FOR ALL)","ADMIN","2019-12-29 10:12:19.963","669","2019-12-30 06:47:24.197",null,"52FA6D7F-12B4-49AF-A11D-1428801E37E1")
 INSERT sy_report_col(ereport,ecolname,ccolname,exp,orderby,width,coltype,colalign,col_flg,col_visible,col_seq,col_key,col_color,colcass,sumcmd,created_by,created_date,mod_by,mod_date,selsql,guid) values("VALIDANALY","saleqty","当日销售数量","sum(a.saleqty)",85,300,"###0.00","1","Y","Y","N","N","""67108864~tif(mod(getrow(),2)=0,rgb(223,234,207),rgb(255,255,255))""",null,null,"ADMIN","2019-12-29 10:05:11.897","669","2019-12-30 06:47:24.193",null,"734DC638-D067-4702-BBFE-65D34EFA4E0A")
 INSERT sy_report_col(ereport,ecolname,ccolname,exp,orderby,width,coltype,colalign,col_flg,col_visible,col_seq,col_key,col_color,colcass,sumcmd,created_by,created_date,mod_by,mod_date,selsql,guid) values("VALIDANALY","smatarea","产地","max(c.smatarea)",30,400,null,"0","Y"," [商品-smatno]","N","N","""67108864~tif(mod(getrow(),2)=0,rgb(223,234,207),rgb(255,255,255))""","char",null,"ADMIN","2019-12-29 10:29:03.103","669","2019-12-30 06:47:24.170",null,"6CE92574-87A4-4FC1-94FE-D8BD42044D41")
 INSERT sy_report_col(ereport,ecolname,ccolname,exp,orderby,width,coltype,colalign,col_flg,col_visible,col_seq,col_key,col_color,colcass,sumcmd,created_by,created_date,mod_by,mod_date,selsql,guid) values("VALIDANALY","smatname","商品名称","max(c.smatname)",20,600,null,"0","Y"," [商品-smatno]","N","N","""67108864~tif(mod(getrow(),2)=0,rgb(223,234,207),rgb(255,255,255))""","char",null,"ADMIN","2019-12-29 10:29:02.777","669","2019-12-30 06:47:24.163",null,"68EB719D-31F6-4D6F-B6DC-CFC05CEF2CE2")
 INSERT sy_report_col(ereport,ecolname,ccolname,exp,orderby,width,coltype,colalign,col_flg,col_visible,col_seq,col_key,col_color,colcass,sumcmd,created_by,created_date,mod_by,mod_date,selsql,guid) values("VALIDANALY","smatno","商品","a.smatno",15,200,null,"0","N","Y","N","N","""67108864~tif(mod(getrow(),2)=0,rgb(223,234,207),rgb(255,255,255))""","char",null,"ADMIN","2019-12-29 09:55:19.533","669","2019-12-30 06:47:24.160",null,"420D6156-F464-4B4A-8D6A-EC4F09798F5C")
 INSERT sy_report_col(ereport,ecolname,ccolname,exp,orderby,width,coltype,colalign,col_flg,col_visible,col_seq,col_key,col_color,colcass,sumcmd,created_by,created_date,mod_by,mod_date,selsql,guid) values("VALIDANALY","smatspec","商品规格","max(c.smatspec)",35,500,null,"0","Y"," [商品-smatno]","N","N","""67108864~tif(mod(getrow(),2)=0,rgb(223,234,207),rgb(255,255,255))""","char",null,"ADMIN","2019-12-29 10:29:03.270","669","2019-12-30 06:47:24.170",null,"4009938E-C91D-4F82-B562-671D98E148C4")
 INSERT sy_report_col(ereport,ecolname,ccolname,exp,orderby,width,coltype,colalign,col_flg,col_visible,col_seq,col_key,col_color,colcass,sumcmd,created_by,created_date,mod_by,mod_date,selsql,guid) values("VALIDANALY","smatunit","单位","max(c.smatunit)",40,100,null,"0","Y"," [商品-smatno]","N","N","""67108864~tif(mod(getrow(),2)=0,rgb(223,234,207),rgb(255,255,255))""","char",null,"ADMIN","2019-12-29 10:29:03.417","669","2019-12-30 06:47:24.173",null,"D03C6F14-7EAB-4916-AB49-45E2C49F65DC")
 INSERT sy_report_col(ereport,ecolname,ccolname,exp,orderby,width,coltype,colalign,col_flg,col_visible,col_seq,col_key,col_color,colcass,sumcmd,created_by,created_date,mod_by,mod_date,selsql,guid) values("VALIDANALY","stkamt","结余进价金额","sum(a.stockqty*b.incomprice)",80,300,"###0.00","1","Y","Y","N","N","""67108864~tif(mod(getrow(),2)=0,rgb(223,234,207),rgb(255,255,255))""",null,"SUM(stkamt FOR ALL)","ADMIN","2019-12-29 10:07:14.470","669","2019-12-30 06:47:24.190",null,"5AC109AA-C1BB-4217-80BA-2D1E08F2817F")
 INSERT sy_report_col(ereport,ecolname,ccolname,exp,orderby,width,coltype,colalign,col_flg,col_visible,col_seq,col_key,col_color,colcass,sumcmd,created_by,created_date,mod_by,mod_date,selsql,guid) values("VALIDANALY","stockqty","当日结余库存","sum(a.stockqty)",75,350,"###0.00","1","Y","Y","N","N","""67108864~tif(mod(getrow(),2)=0,rgb(223,234,207),rgb(255,255,255))""",null,null,"ADMIN","2019-12-29 10:05:11.743","669","2019-12-30 06:47:24.190",null,"54A30B59-EC50-418C-9BE2-1386F23E0978")
 INSERT sy_report_col(ereport,ecolname,ccolname,exp,orderby,width,coltype,colalign,col_flg,col_visible,col_seq,col_key,col_color,colcass,sumcmd,created_by,created_date,mod_by,mod_date,selsql,guid) values("VALIDANALY","syamt","损益进价金额","sum(a.syqty*b.incomprice)",120,300,"###0.00","1","Y","Y","N","N","""67108864~tif(mod(getrow(),2)=0,rgb(223,234,207),rgb(255,255,255))""",null,"SUM(syamt FOR ALL)","ADMIN","2019-12-29 10:12:20.447","669","2019-12-30 06:47:24.210",null,"197C3938-D440-43C3-9BEF-09D98F38FC34")
 INSERT sy_report_col(ereport,ecolname,ccolname,exp,orderby,width,coltype,colalign,col_flg,col_visible,col_seq,col_key,col_color,colcass,sumcmd,created_by,created_date,mod_by,mod_date,selsql,guid) values("VALIDANALY","syqty","当日损益数量","sum(a.syqty)",115,300,"###0.00","1","Y","Y","N","N","""67108864~tif(mod(getrow(),2)=0,rgb(223,234,207),rgb(255,255,255))""",null,null,"ADMIN","2019-12-29 10:05:12.347","669","2019-12-30 06:47:24.207",null,"0653D5D0-BD0C-49D9-94C6-E357DF928D8D")
 INSERT sy_report_col(ereport,ecolname,ccolname,exp,orderby,width,coltype,colalign,col_flg,col_visible,col_seq,col_key,col_color,colcass,sumcmd,created_by,created_date,mod_by,mod_date,selsql,guid) values("VALIDANALY","validdate","效期","convert(varchar(10),b.validdate,112)",60,300,null,"0","N","Y","N","N","""67108864~tif(mod(getrow(),2)=0,rgb(223,234,207),rgb(255,255,255))""","char",null,"ADMIN","2019-12-29 10:05:11.293","669","2019-12-30 06:47:24.180",null,"51D2ACE0-AF1E-4D5C-BAE7-39F0BF41A161")
 INSERT sy_report_col(ereport,ecolname,ccolname,exp,orderby,width,coltype,colalign,col_flg,col_visible,col_seq,col_key,col_color,colcass,sumcmd,created_by,created_date,mod_by,mod_date,selsql,guid) values("VALIDANALY","validday","效期天数","a.validday",50,300,null,"0","N","Y","N","N","""67108864~tif(mod(getrow(),2)=0,rgb(223,234,207),rgb(255,255,255))""","deci",null,"ADMIN","2019-12-29 09:55:19.823","669","2019-12-30 06:47:24.177",null,"9F2DAE02-0C66-47A0-B5FE-4D60ACBF414A")
 INSERT sy_report_col(ereport,ecolname,ccolname,exp,orderby,width,coltype,colalign,col_flg,col_visible,col_seq,col_key,col_color,colcass,sumcmd,created_by,created_date,mod_by,mod_date,selsql,guid) values("VALIDANALY","validdesc","效期段落","a.validdesc",55,400,null,"0","N","Y","N","N","""67108864~tif(mod(getrow(),2)=0,rgb(223,234,207),rgb(255,255,255))""","char",null,"ADMIN","2019-12-29 09:55:19.970","669","2019-12-30 06:47:24.180","A(30天以下)/B(30-60天)/C(60-90天)/D(90-180天)/E(181-270天)/","3076CCB3-600A-4E0D-AE91-F21FA7288558")
 INSERT sy_report_col(ereport,ecolname,ccolname,exp,orderby,width,coltype,colalign,col_flg,col_visible,col_seq,col_key,col_color,colcass,sumcmd,created_by,created_date,mod_by,mod_date,selsql,guid) values("VALIDANALY","ymd","日期","a.ymd",5,300,null,"0","N","Y","N","N","""67108864~tif(mod(getrow(),2)=0,rgb(223,234,207),rgb(255,255,255))""","char",null,"ADMIN","2019-12-29 09:55:19.240","669","2019-12-30 06:47:24.157","DATE","3614CEB2-C491-457B-AC08-63EE07EA1ED9")
 end
 go
 IF NOT EXISTS (SELECT 1 FROM sy_menus WHERE name = "效期动销分析报表")
 BEGIN
 DECLARE @MENUID INT
 SELECT @MENUID = MAX(MENU_ID)+1 FROM sy_menus
 INSERT sy_menus(menu_id,name,ename,descr,edescr,order_by,icon,is_prg,prg_code,prg_source,abc,status,pa_menu_id,created_by,created_date,mod_by,mod_date,guid,sign_no) values(@MENUID,"效期动销分析报表",null,null,null,130,"Report!","R",null,"W_REPORT","XQDXFXBB","Y",49,"ADMIN","2019-12-29 09:39:45.983","ADMIN","2019-12-29 09:40:33.690","F9A14EC9-2697-4A1B-8DDE-D60C7B3AE1F9",null)
 INSERT sy_mnu_items(menu_id,item_no,name,ename,descr,edescr,winobject,mnutype,toolbar_pic,auth_flg,groupid,dwcolnum,created_by,created_date,mod_by,mod_date,guid,sqltext,execflg) values(@MENUID,6,"查询","-","ue_find","CTRL+Q","uo_toolbar_1","1","bmp\Search.ico","Y",0,"",null,NULL,null,NULL,"869BD3D4-B398-4C20-8640-FF7880CFBF26",null,0)
 INSERT sy_mnu_items(menu_id,item_no,name,ename,descr,edescr,winobject,mnutype,toolbar_pic,auth_flg,groupid,dwcolnum,created_by,created_date,mod_by,mod_date,guid,sqltext,execflg) values(@MENUID,32,"分解分析","-","ue_datastru","CTRL+G","uo_toolbar_1","1","EditDataGrid!","N",0,"",null,NULL,null,NULL,"6291391E-3ACC-4BC0-9C20-6E0165D3EB92",null,0)
 INSERT sy_mnu_items(menu_id,item_no,name,ename,descr,edescr,winobject,mnutype,toolbar_pic,auth_flg,groupid,dwcolnum,created_by,created_date,mod_by,mod_date,guid,sqltext,execflg) values(@MENUID,33,"对比分析","","ue_comays","CTRL+J","uo_toolbar_1","1","Globals!","N",0,"",null,NULL,null,NULL,"7CCE2291-E8B6-49A5-89B6-75481EFE8EA8",null,0)
 INSERT sy_mnu_items(menu_id,item_no,name,ename,descr,edescr,winobject,mnutype,toolbar_pic,auth_flg,groupid,dwcolnum,created_by,created_date,mod_by,mod_date,guid,sqltext,execflg) values(@MENUID,34,"最末页","","ue_pgdne","CTRL+RIGHT","uo_toolbar_1","1","VCRLast!","N",0,"",null,NULL,null,NULL,"A9820B00-4FE8-4D14-A8F0-572F5A9F53D4",null,0)
 INSERT sy_mnu_items(menu_id,item_no,name,ename,descr,edescr,winobject,mnutype,toolbar_pic,auth_flg,groupid,dwcolnum,created_by,created_date,mod_by,mod_date,guid,sqltext,execflg) values(@MENUID,35,"下一页","","ue_pgdn","RIGHT","uo_toolbar_1","1","VCRNext!","N",0,"",null,NULL,null,NULL,"2F89A140-FF99-4A0B-B816-8ACF43A0E50F",null,0)
 INSERT sy_mnu_items(menu_id,item_no,name,ename,descr,edescr,winobject,mnutype,toolbar_pic,auth_flg,groupid,dwcolnum,created_by,created_date,mod_by,mod_date,guid,sqltext,execflg) values(@MENUID,36,"上一页","","ue_pgup","LEFT","uo_toolbar_1","1","VCRPrior!","N",0,"",null,NULL,null,NULL,"64DB5A05-5B5B-449B-924C-5769BD902BEF",null,0)
 INSERT sy_mnu_items(menu_id,item_no,name,ename,descr,edescr,winobject,mnutype,toolbar_pic,auth_flg,groupid,dwcolnum,created_by,created_date,mod_by,mod_date,guid,sqltext,execflg) values(@MENUID,37,"第一页","","ue_pgupb","CTRL+LEFT","uo_toolbar_1","1","VCRFirst!","N",0,"",null,NULL,null,NULL,"25A42684-C677-4D54-AC83-CF062BFC9340",null,0)
 
 UPDATE sy_report
 SET    menu_id = @MENUID
 WHERE  ereport = "VALIDANALY"
 END