set ansi_nulls on
go
set quoted_identifier off
go
if OBJECT_ID('pro_get_vipshop') is not null drop proc pro_get_vipshop
go  
create PROC pro_get_vipshop(@vipno varchar(30),@shoptype int)  
as  
DECLARE @NL        VARCHAR(10)  
DECLARE @YL        VARCHAR(10)  
DECLARE @WK        VARCHAR(10)  
declare @VDATETIME varchar(20)  
declare @VSRFLG    varchar(10)  
declare @shophdno  varchar(30)  
declare @viphw     varchar(2000)  
declare @sql       varchar(2000)  
declare @vipljcs   int  
declare @vipflag   int  
declare @cnt   int  
  
set @VDATETIME = convert(varchar(10),getdate(),112)+REPLACE(LEFT(convert(varchar(10),getdate(),108),5),':','')    
  
  
SELECT @NL = RIGHT(dbo.FUN_GETLUNAR(GETDATE()),2)  ,  
       @YL = RIGHT(convert(varchar(10),GETDATE(),112),2)  ,  
       @WK = CAST(DATEPART(WEEKDAY, GETDATE())- 1 AS VARCHAR(2))   
         
SELECT @VSRFLG =CASE BIRTHDAYTYPE WHEN 1 THEN CASE RIGHT(convert(varchar(10),BIRTHDAY,112),4) WHEN RIGHT(convert(varchar(10),GETDATE(),112),4) THEN 'SR'   
                                                                                     ELSE '/'   
                                     END           
                         WHEN 2 THEN CASE RIGHT(LUNARDAY,4)  WHEN RIGHT(dbo.FUN_GETLUNAR(GETDATE()),4)   THEN 'SR'   
                                                             ELSE '/'   
                                     END           
                         ELSE '/'    
        END  
FROM    hy_vip_m   
WHERE   vipno = @VIPNO    
  
If object_id('tempdb..#hy_shop_hd') is not null drop table #hy_shop_hd  
select *  
into   #hy_shop_hd  
from   hy_shop_hd   
where  shophdtype = @shoptype and   
       CONVERT(varchar(8),shophdbdate ,112)+shopbtime<=@VDATETIME and  
       CONVERT(varchar(8),shophdedate ,112)+shopetime>=@VDATETIME and  
       (PROMODATE LIKE (CASE PROMOTYPE WHEN 1 THEN '%'+@YL+'%' WHEN 2 THEN '%'+@NL+'%' WHEN 3 THEN '%'+@WK+'%' WHEN 4 THEN @VSRFLG END) OR promotype = '0') and  
        status = 1  and  
       (zcash = 0 or (zcash > 0 and zcash > ycash) )  
        
--处理会员范围  
set @shophdno = ''  
If object_id('tempdb..#hy_vip') is not null drop table #hy_vip  
select vipno  
into   #hy_vip  
from   hy_vip_m  
where  1=0  
  
while 0=0  
begin  
select top 1 @shophdno = shophdno,  
             @viphw = 'insert into #hy_vip '+ viphw,  
             @vipljcs = vipljcs,  
             @vipflag = vipflag  
from   #hy_shop_hd    
where  shophdno > @shophdno  
order by shophdno  
  
IF @@ROWCOUNT = 0 BREAK  
  
select  @cnt = COUNT(shopcarddesc)  
from    hy_shopcard  
where   shopcarddesc = @shophdno and vipno = @vipno   
if @vipljcs = 0 and @cnt > 0   
 begin  
 delete from  #hy_shop_hd  
 where  shophdno = @shophdno  
 end  
else if @vipljcs <= @cnt and @vipljcs > 0   
    begin  
 delete from  #hy_shop_hd  
 where  shophdno = @shophdno  
 end  
else   
begin  
 if @vipflag = 2   
 begin  
  DELETE FROM #hy_vip  
  set @viphw = REPLACE(@viphw,'@VIPNO',@vipno)  
  EXEC(@viphw)  
  if not exists (select 1 from #hy_vip where vipno = @vipno)   
  begin  
  delete from  #hy_shop_hd  
  where  shophdno = @shophdno  
  end  
 end   
end  
  
end    
  
select *  
from   #hy_shop_hd  
go
if OBJECT_ID('pro_ins_vipshop') is not null drop proc pro_ins_vipshop
go
create PROC pro_ins_vipshop(@shophdno varchar(30),@vipno varchar(30),@shoptype int)
as

declare @amte       numeric(16,2)
declare @amtb       numeric(16,2)
declare @shopamt    numeric(16,2)
declare @shopcardno varchar(30) 
declare @organno    varchar(10)
declare @bill_date  varchar(10)
declare @zcash      numeric(16,2)
declare @ycash      numeric(16,2)

--判断此活动单号此会员是否可用
If object_id('tempdb..#shop_hd') is not null drop table #shop_hd
select *
into   #shop_hd
from   hy_shop_hd 
where  1=0
insert into #shop_hd  exec pro_get_vipshop @vipno,@shoptype
if not exists (select 1 from #shop_hd where shophdno = @shophdno)
begin
raiserror 20012 '请您确定是否满足此领奖单的条件!!'                      
return  
end

--得到购物卡金额
select @amte = amte,@amtb = amtb,@zcash = zcash ,@ycash= ycash
from   hy_shop_hd
where  shophdno  = @shophdno
select @shopamt = cast(rand() * @amte as numeric(12,1)) 
if @amtb    > @shopamt set @shopamt  = @amtb
if @zcash > 0
begin
if @shopamt > @zcash - @ycash   set @shopamt  = @zcash - @ycash
end
--产生单号
SET @organno = DBO.fun_get_ini('1001')
select @bill_date = substring(aa,3,6)  from (select convert(char(10),getdate(),112) as aa) tab 
exec pro_bill_code 'C7',@organno,@bill_date,'','','','',@shopcardno out           
if @@error <> 0                  
begin                   
   rollback                  
   raiserror 20003 '产生购物卡号出错'                  
   return                  
end  

--插入购物卡
insert into hy_shopcard(shopcardno,shopcarddesc,organno,shopamt,cpdamt,begindate,enddate,minsalecash,vipcardflag,status,checkdate,checkby,
                        prertype,smatno,groupid,comlevel,sortcode,vipno,remark,created_by,created_date,mod_by,mod_date,czby,cztel,ycxf) 
select  @shopcardno,shophdno,@organno,@shopamt,0,begindate,enddate,minsalecash,vipcardflag,1,GETDATE(),'WX',
        prertype,smatno,groupid,comlevel,sortcode ,@vipno,shophddescr,'WX',GETDATE(),NULL,NULL,NULL,NULL,ycxf
from    hy_shop_hd  
where   shophdno  = @shophdno 
if @@error <> 0                  
begin                   
   rollback                  
   raiserror 20003 '产生购物卡出错'                  
   return                  
end  

--修改已领金额
UPDATE  hy_shop_hd
SET     ycash = ISNULL(ycash,0)+@shopamt      
where   shophdno  = @shophdno 
if @@error <> 0                  
begin                   
   rollback                  
   raiserror 20003 '修改活动单号出错'                  
   return                  
end                     
go  
alter table  pf_tradsale_d alter column  groupid varchar(100)
go

if OBJECT_ID('vw_awaitqty') is not null drop view vw_awaitqty
go

 ----待出库清单    
 ----单据类型,单据号,业务机构,机构名称,商品编码,批号,有效期,货位,待出库数量,创建人,批次识别号             
create  view vw_awaitqty as             
--退仓申请单   
select billno,a.applyno as billcode,srcorganno as organno,rtrim(c.organno) + c.organname as objname,b.smatno,b.lotno ,b.validdate ,shelfno ,appstockqty as stockqty,a.created_by,batchno,idno  
from   ps_moveapply_m a (nolock),ps_moveapply_d b (nolock) ,ba_organ c (nolock)      
where  a.status in (0,1) and a.applyno = b.applyno and a.srcorganno = c.organno and billno='APR'    
union all   
--店间调拨申请单  
select billno,a.applyno as billcode,srcorganno,rtrim(c.organno) + c.organname as objname,b.smatno,b.lotno ,b.validdate ,shelfno ,appstockqty,a.created_by,batchno,idno 
from   ps_moveapply_m a (nolock),ps_moveapply_d b (nolock) ,ba_organ c (nolock)      
where  a.status in (0,1) and a.applyno = b.applyno and a.srcorganno = c.organno and billno='APM'   
union all    
-----配送退仓移库货位调整加盟调拨加盟调拨退回   
select a.billno,a.moveno as billcode,a.srcorganno,rtrim(c.organno) + c.organname as objname,b.smatno,b.lotno,b.validdate ,  
b.srcshelfno as shelfno ,stockqty, a.created_by ,batchno,idno 
from   ps_move_m a (nolock),ps_move_d b (nolock) ,ba_organ c (nolock)      
where  a.status =0 and a.moveno = b.moveno and a.objorganno = c.organno   
union all   
--供应商退货单  
select billno,a.recno as billcode,organno,rtrim(c.supno) + c.supname as objname,b.smatno,b.lotno ,b.validdate ,shelfno ,stockqty,  a.created_by ,old_batchno ,old_idno
from cg_sup_m a (nolocK),cg_sup_d b(nolock),ba_supplier c(nolock)            
where a.recno = b.recno and a.status = 0 and a.billno = 'RET' and a.supno = c.supno                   
union all         
--采购退货通知单  
select billno,a.applyno as billcode,organno,rtrim(c.supno) + c.supname as objname,b.smatno,b.lotno ,b.validdate ,shelfno ,stockqty,  a.created_by ,old_batchno,old_idno
from cg_supapply_m a (nolocK),cg_supapply_d b(nolock),ba_supplier c(nolock)            
where a.applyno = b.applyno and a.status in (0,1) and a.billno = 'BET' and a.supno = c.supno    
union all     
--报损单  
select billno,a.incomeno as billcode,a.organno,rtrim(c.organno) + c.organname as objname,b.smatno,b.lotno ,b.validdate ,shelfno ,b.stockqtyo - b.stockqtyn as stockqty,    
a.created_by ,batchno,idno
from st_income_m a (nolocK),st_income_d b(nolock),ba_organ c(nolock)            
where a.incomeno = b.incomeno and a.status = 0 and b.stockqtyo - b.stockqtyn > 0  and a.organno=c.organno    
union all     
--报损申请单  
select billno,a.applyno as billcode,a.organno,rtrim(c.organno) + c.organname as objname,b.smatno,b.lotno ,b.validdate ,shelfno ,
b.oldqty - b.newqty as stockqty,    a.created_by ,batchno,idno
from ps_income_m a (nolocK),ps_income_d b(nolock),ba_organ c(nolock)            
where a.applyno = b.applyno and a.status In(0,1) and b.oldqty - b.newqty > 0  and a.organno=c.organno    
union all  
--质量复查单，来源不为购进验收。  
select billno,a.moveno as billcode,srcorganno,rtrim(c.organno) + c.organname as objname,b.smatno,b.lotno ,b.validdate ,  
srcshelfno,stockqty,  a.created_by ,batchno,idno  
from   zl_move_m a (nolock),zl_move_d b (nolock) ,ba_organ c (nolock)      
where  a.status =0 and a.moveno = b.moveno and billno='ZLE' and srcorganno= c.organno  and a.orgsource<>'01'
union all
--批发单
select  billno,a.tradno as billcode,a.organno, rtrim(c.organno) + c.organname as objname,b.smatno,b.lotno ,b.validdate ,  
b.shelfno,b.stockqty,  a.created_by ,b.batchno,b.idno 
from  pf_tradsale_m a,pf_tradsale_d b ,ba_organ c (nolock) 
where a.tradno = b.tradno  and a.status = 0  and a.billno = 'TRA'  and a.organno = c.organno
union all
--直营门店收货/验收单
select  a.billno,a.moveno as billcode,a.objorganno as organno,rtrim(c.organno) + c.organname as objname,b.smatno,b.lotno,
b.validdate,b.objshelfno as shelfno,b.stockqty,a.mod_by as created_by,b.batchno,b.idno
from ps_accept_m a(nolock),ps_move_d b(nolock),ba_organ c (nolock) ,ps_move_m d(nolock)
where a.movecode = b.moveno  and a.status = 0  and a.objorganno = c.organno and b.moveno like 'DIT%'
and  b.moveno = d.moveno  and isnull(d.remark,'') not like '%店间调拨%'
union all
--加盟门店收货/验收单
select  a.billno,a.moveno as billcode,a.objorganno as organno,rtrim(c.organno) + c.organname as objname,b.smatno,b.lotno,
b.validdate,b.objshelfno as shelfno,b.stockqty,a.mod_by as created_by,b.objbatchno,b.objidno
from ps_accept_m a(nolock),ps_move_d b(nolock),ba_organ c (nolock)
where a.movecode = b.moveno  and a.status = 0  and a.objorganno = c.organno and b.moveno like 'DUT%'
union all
--挂单根据4056参数控制是否管代出库
SELECT   'GD',a.saleputno,a.organno,Rtrim(a.organno) + c.organname as objname,b.smatno,b.lotno,b.validdate,
       b.shelfno,ROUND(CASE WHEN B.ISMINQTY = 1 THEN B.QTY/B.CATTERED_PER ELSE B.QTY END,4) AS stockqty,
       A.CREATED_BY,B.BATCHNO,B.IDNO
FROM     ls_saleput_m a join ls_saleput_d b on a.saleputno = b.saleputno
                      join ba_organ     c on a.organno   = c.organno
WHERE    flag <> 1  and exists(select ini from sy_ini  where ini = '4056'  and isnull(rtrim(ltrim(para)),'') = '1')


GO


alter table  hy_shop_hd alter column shophdname varchar(1000)
if COL_LENGTH('hy_shopcard','shophddescr') is null
alter table  hy_shopcard add  shophddescr varchar(300)
if COL_LENGTH('hy_shopcard','shophdname') is null
alter table  hy_shopcard add  shophdname varchar(1000)
go
if OBJECT_ID('pro_ins_vipshop') is not null drop proc pro_ins_vipshop
go
create PROC pro_ins_vipshop(@shophdno varchar(30),@vipno varchar(30),@shoptype int)
as

declare @amte       numeric(16,2)
declare @amtb       numeric(16,2)
declare @shopamt    numeric(16,2)
declare @shopcardno varchar(30) 
declare @organno    varchar(10)
declare @bill_date  varchar(10)
declare @zcash      numeric(16,2)
declare @ycash      numeric(16,2)

--判断此活动单号此会员是否可用
If object_id('tempdb..#shop_hd') is not null drop table #shop_hd
select shophdno,shophddescr,shophdtype,shophdbdate,shopbtime,shophdedate,shopetime,promotype,promodate,vipflag,viphw,amtb,amte,zcash,ycash,vipljcs,begindate,enddate,minsalecash,vipcardflag,prertype,smatno,groupid,comlevel,sortcode,ycxf,status,checkdate,checkby,shophdname  
into   #shop_hd
from   hy_shop_hd 
where  1=0
insert into #shop_hd  exec pro_get_vipshop @vipno,@shoptype
if not exists (select 1 from #shop_hd where shophdno = @shophdno)
begin
raiserror 20012 '请您确定是否满足此领奖单的条件!!'                      
return  
end

--得到购物卡金额
select @amte = amte,@amtb = amtb,@zcash = zcash ,@ycash= ycash
from   hy_shop_hd
where  shophdno  = @shophdno
select @shopamt = cast(rand() * @amte as numeric(12,1)) 
if @amtb    > @shopamt set @shopamt  = @amtb
if @zcash > 0
begin
if @shopamt > @zcash - @ycash   set @shopamt  = @zcash - @ycash
end
--产生单号
SET @organno = DBO.fun_get_ini('1001')
select @bill_date = substring(aa,3,6)  from (select convert(char(10),getdate(),112) as aa) tab 
exec pro_bill_code 'C7',@organno,@bill_date,'','','','',@shopcardno out           
if @@error <> 0                  
begin                   
   rollback                  
   raiserror 20003 '产生购物卡号出错'                  
   return                  
end  

--插入购物卡
insert into hy_shopcard(shopcardno,shopcarddesc,organno,shopamt,cpdamt,begindate,enddate,minsalecash,vipcardflag,status,checkdate,checkby,
                        prertype,smatno,groupid,comlevel,sortcode,vipno,remark,created_by,created_date,mod_by,mod_date,czby,cztel,ycxf,shophddescr,shophdname) 
select  @shopcardno,shophdno,@organno,@shopamt,0,begindate,enddate,minsalecash,vipcardflag,1,GETDATE(),'WX',
        prertype,smatno,groupid,comlevel,sortcode ,@vipno,shophddescr,'WX',GETDATE(),NULL,NULL,NULL,NULL,ycxf,shophddescr,shophdname
from    hy_shop_hd  
where   shophdno  = @shophdno 
if @@error <> 0                  
begin                   
   rollback                  
   raiserror 20003 '产生购物卡出错'                  
   return                  
end  

--修改已领金额
UPDATE  hy_shop_hd
SET     ycash = ISNULL(ycash,0)+@shopamt      
where   shophdno  = @shophdno 
if @@error <> 0                  
begin                   
   rollback                  
   raiserror 20003 '修改活动单号出错'                  
   return                  
end                     
go
if OBJECT_ID('pro_get_vipshop') is not null drop proc pro_get_vipshop
go
create PROC pro_get_vipshop(@vipno varchar(30),@shoptype int)  
as  
DECLARE @NL        VARCHAR(10)  
DECLARE @YL        VARCHAR(10)  
DECLARE @WK        VARCHAR(10)  
declare @VDATETIME varchar(20)  
declare @VSRFLG    varchar(10)  
declare @shophdno  varchar(30)  
declare @viphw     varchar(2000)  
declare @sql       varchar(2000)  
declare @vipljcs   int  
declare @vipflag   int  
declare @cnt   int  
  
set @VDATETIME = convert(varchar(10),getdate(),112)+REPLACE(LEFT(convert(varchar(10),getdate(),108),5),':','')    
  
  
SELECT @NL = RIGHT(dbo.FUN_GETLUNAR(GETDATE()),2)  ,  
       @YL = RIGHT(convert(varchar(10),GETDATE(),112),2)  ,  
       @WK = CAST(DATEPART(WEEKDAY, GETDATE())- 1 AS VARCHAR(2))   
         
SELECT @VSRFLG =CASE BIRTHDAYTYPE WHEN 1 THEN CASE RIGHT(convert(varchar(10),BIRTHDAY,112),4) WHEN RIGHT(convert(varchar(10),GETDATE(),112),4) THEN 'SR'   
                                                                                     ELSE '/'   
                                     END           
                         WHEN 2 THEN CASE RIGHT(LUNARDAY,4)  WHEN RIGHT(dbo.FUN_GETLUNAR(GETDATE()),4)   THEN 'SR'   
                                                             ELSE '/'   
                                     END           
                         ELSE '/'    
        END  
FROM    hy_vip_m   
WHERE   vipno = @VIPNO    
  
If object_id('tempdb..#hy_shop_hd') is not null drop table #hy_shop_hd  
select *  
into   #hy_shop_hd  
from   hy_shop_hd   
where  shophdtype = @shoptype and   
       CONVERT(varchar(8),shophdbdate ,112)+shopbtime<=@VDATETIME and  
       CONVERT(varchar(8),shophdedate ,112)+shopetime>=@VDATETIME and  
       (PROMODATE LIKE (CASE PROMOTYPE WHEN 1 THEN '%'+@YL+'%' WHEN 2 THEN '%'+@NL+'%' WHEN 3 THEN '%'+@WK+'%' WHEN 4 THEN @VSRFLG END) OR promotype = '0') and  
        status = 1  and  
       (zcash = 0 or (zcash > 0 and zcash > ycash) )  
        
--处理会员范围  
set @shophdno = ''  
If object_id('tempdb..#hy_vip') is not null drop table #hy_vip  
select vipno  
into   #hy_vip  
from   hy_vip_m  
where  1=0  
  
while 0=0  
begin  
select top 1 @shophdno = shophdno,  
             @viphw = 'insert into #hy_vip '+ viphw,  
             @vipljcs = vipljcs,  
             @vipflag = vipflag  
from   #hy_shop_hd    
where  shophdno > @shophdno  
order by shophdno  
  
IF @@ROWCOUNT = 0 BREAK  
  
select  @cnt = COUNT(shopcarddesc)  
from    hy_shopcard  
where   shopcarddesc = @shophdno and vipno = @vipno   
if @vipljcs = 0 and @cnt > 0   
 begin  
 delete from  #hy_shop_hd  
 where  shophdno = @shophdno  
 end  
else if @vipljcs <= @cnt and @vipljcs > 0   
    begin  
 delete from  #hy_shop_hd  
 where  shophdno = @shophdno  
 end  
else   
begin  
 if @vipflag = 2   
 begin  
  DELETE FROM #hy_vip  
  set @viphw = REPLACE(@viphw,'@VIPNO',@vipno)  
  EXEC(@viphw)  
  if not exists (select 1 from #hy_vip where vipno = @vipno)   
  begin  
  delete from  #hy_shop_hd  
  where  shophdno = @shophdno  
  end  
 end   
end  
  
end    
  
select shophdno,shophddescr,shophdtype,shophdbdate,shopbtime,shophdedate,shopetime,promotype,promodate,vipflag,viphw,amtb,amte,zcash,ycash,vipljcs,begindate,enddate,minsalecash,vipcardflag,prertype,smatno,groupid,comlevel,sortcode,ycxf,status,checkdate,checkby,shophdname  
from   #hy_shop_hd  
go
if OBJECT_ID('pro_plan_hb') is not null drop proc pro_plan_hb
go
create proc pro_plan_hb(@user_code varchar(10),@outbillno varchar(30) out )
as
declare @bill_date varchar(6)
declare @organno   varchar(10)
declare @vreturn varchar(30)

if not exists (select 1 from #plan_hb)
begin
raiserror 20001 '请选择要合并的采购计划单!'          
return  
end

select top 1 @organno = organno
from   cg_plan_m
where  exists (select 1 from #plan_hb where planno = cg_plan_m.planno)   



select @bill_date = substring(aa,3,6)  from (select convert(char(8),getdate(),112) as aa) tab

exec pro_bill_code '71',@organno,@bill_date,'','','','',@outbillno out 
if @@error <> 0                  
begin                   
	rollback                  
	raiserror 20012 '产生采购计划单单号出错！'                  
	return                  
end
     
INSERT INTO cg_plan_m(planno, organno, supplyer, status,remark,checkdate,created_by,created_date,mod_by,mod_date,billcode)
SELECT  TOP 1 @outbillno,organno, supplyer, 0,'合并生成',null,@user_code,GETDATE(),null,null,@outbillno
from   cg_plan_m
where  exists (select 1 from #plan_hb where planno = cg_plan_m.planno)  

exec pro_sign @outbillno,'71',@user_code,'I',@vreturn out 
if  @vreturn <> 'Y'  
begin 
rollback 
raiserror 20007 '插入签核资料出错！'   
return 
end   

if object_id('tempdb..#plan_d')  is not null drop table #plan_d  
select planno, supno,idno,paytype,smatno,incomprice,planqty,orderno,stockqty,maxqty,minqty,
                      saleprice,incomtax,whqty,wayqty,sumawaitqty,lastmqty,lastm2qty,lastm3qty,
                      lastyqty,tradprice,miniprice,deptno,flag,remark,identity(int,1,1) as cnt
into   #plan_d
from   cg_plan_d
where  exists (select 1 from #plan_hb where planno = cg_plan_d.planno)

update #plan_d
set    idno = right('000000'+CAST(cnt as varchar(10)),6)

INSERT INTO cg_plan_d(planno, supno,idno,paytype,smatno,incomprice,planqty,orderno,stockqty,maxqty,minqty,
                      saleprice,incomtax,whqty,wayqty,sumawaitqty,lastmqty,lastm2qty,lastm3qty,
                      lastyqty,tradprice,miniprice,deptno,flag,remark)	
select    @outbillno, supno,idno,paytype,smatno,incomprice,planqty,orderno,stockqty,maxqty,minqty,
          saleprice,incomtax,whqty,wayqty,sumawaitqty,lastmqty,lastm2qty,lastm3qty,
          lastyqty,tradprice,miniprice,deptno,flag,remark
from      #plan_d  

update   a
set      a.status = 2,a.remark = '由计划单:'+@outbillno+'合并生成'
from     cg_plan_m a
where    exists (select 1 from #plan_hb where planno = a.planno)         

go

if OBJECT_ID('pro_rf_to_check') is not null drop proc pro_rf_to_check
go 

create PROC pro_rf_to_check(@checkno varchar(30))
as

declare @organno varchar(10)               
declare @msg varchar(100)  
declare @smatno varchar(13)
declare @lotno varchar(20)
declare @idnum int 
declare @batchno varchar(30)
declare @idno varchar(20)
declare @shelfno varchar(20)
declare @accqty numeric(16,4)
declare @checkqty numeric(16,4)           
declare @count int
declare @created_by varchar(40)
declare @bill_date varchar(6)
declare @checkorganno varchar(10)
declare @id_no varchar(30)
    

set @checkorganno = dbo.fun_get_ini('1001') 
select @bill_date = substring(aa,3,6)  from (select convert(char(8),getdate(),112) as aa) tab    


If not exists(select * from st_check_rf where checkno=@checkno and status = 0 )              
Begin              
        raiserror 20001 '没有要导入的数据！'              
        return              
End        

      
begin tran        
            
        
select @msg='盘点机导入明细中第' + cast(idnum as varchar(10)) + '行' + rtrim(a.smatno) + '在登记帐存表中不存在，无法导入'  
from st_check_rf a 
where not exists(select b.smatno from st_check_acc b where a.smatno=b.smatno  and b.checkno=@checkno)  
and checkno=@checkno
if @msg >''                
  begin                
	rollback                
	raiserror 20002 @msg                
	return                
  end     


if OBJECT_ID('tempdb..#checkrf')  is not null drop table #checkrf


select * into #checkrf 
from st_check_rf where checkno=@checkno and status = 0

	if @@error <> 0                
	  begin             
		rollback                
		raiserror 20003 "插入临时表失败！"                
		return                
	  end 



update    a   set   a.status = 1 
from st_check_rf a,#checkrf b 
where a.checkno=@checkno and a.status = 0  and a.idnum = b.idnum
  	if @@error <> 0                
	  begin             
		rollback                
		raiserror 20004 "更新状态失败！"                
		return                
	  end  
	  
	  
	  
----------------------------------------匹配开始了----------------------------------  
  
if object_id('tempdb..#实盘匹配') is not null drop table #实盘匹配   

create table #实盘匹配(organno varchar(10),smatno varchar(15),batchno varchar(30),idno varchar(12),shelfno varchar(10),stockqty numeric(14,4),created_by varchar(40))   

if object_id('tempdb..#实盘') is not null drop table #实盘

declare cur_check cursor for select organno,smatno,idnum,lotno,checkqty,created_by  from #checkrf  
open cur_check
fetch next from cur_check into @organno,@smatno,@idnum,@lotno,@checkqty,@created_by
while @@FETCH_STATUS = 0
begin

	select @count = COUNT(*)  
	from #checkrf  
	where checkno = @checkno  and smatno = @smatno  and lotno = @lotno 

	declare cur_acc cursor for select batchno,idno,shelfno,accqty  from st_check_acc  
	where checkno = @checkno  and smatno = @smatno  and lotno = @lotno   
	order by accqty desc,batchno desc
	open cur_acc
    fetch next from cur_acc into @batchno,@idno,@shelfno,@accqty
	while @@FETCH_STATUS = 0
	begin

		if @count = 1 
		begin


			insert into #实盘匹配(organno,smatno,batchno,idno,shelfno,stockqty,created_by)
			values(@organno,@smatno,@batchno,@idno,@shelfno,@checkqty,@created_by)
			set @count = 0 
			 
			if @@error <> 0                
                  begin     
                  	close cur_acc
					deallocate cur_acc
                    close cur_check
					deallocate cur_check        
                    rollback                
                    raiserror 20005 "插入实盘匹配失败！"                
                    return                
                  end 
                  
		end

		else if @count>1
		begin
		
			if @accqty<=0
			begin
					insert into #实盘匹配(organno,smatno,batchno,idno,shelfno,stockqty,created_by)
					values(@organno,@smatno,@batchno,@idno,@shelfno,@checkqty,@created_by)	
					
					set @count = 0	
							       
			if @@error <> 0                
                  begin     
                  	close cur_acc
					deallocate cur_acc
                    close cur_check
					deallocate cur_check        
                    rollback                
                    raiserror 20006 "插入实盘匹配失败！"                
                    return                
                  end 
                  
                  
			end
			else
			begin


				if @accqty>=@checkqty
				begin

				insert into #实盘匹配(organno,smatno,batchno,idno,shelfno,stockqty,created_by)
				values(@organno,@smatno,@batchno,@idno,@shelfno,@checkqty,@created_by)
				set @count = 0
				if @@error <> 0                
                  begin     
                  	close cur_acc
					deallocate cur_acc
                    close cur_check
					deallocate cur_check        
                    rollback                
                    raiserror 20007 "插入实盘匹配失败！"                
                    return                
                  end 
                  
                  
				end

				else
				begin


				insert into #实盘匹配(organno,smatno,batchno,idno,shelfno,stockqty,created_by)
				values(@organno,@smatno,@batchno,@idno,@shelfno,@accqty,@created_by)

				set @checkqty = @checkqty - @accqty
				
				set @count = @count - 1
				
				if @@error <> 0                
                  begin     
                  	close cur_acc
					deallocate cur_acc
                    close cur_check
					deallocate cur_check        
                    rollback                
                    raiserror 20008 "插入实盘匹配失败！"                
                    return                
                  end 
                  
                  
                  
				end
			end
			
			
			
		end




	fetch next from cur_acc into @batchno,@idno,@shelfno,@accqty
	end
	close cur_acc
	deallocate cur_acc





fetch next from cur_check into @organno,@smatno,@idnum,@lotno,@checkqty,@created_by
end
close cur_check
deallocate cur_check



select  *,identity(int,1,1) as rowid  into #实盘 
from #实盘匹配
order by smatno,batchno,idno,shelfno

      if @@error <> 0                
          begin                
            rollback                
            raiserror 20009 "插入实盘失败！"                
            return                
          end    




 -------------------------------------------------匹配结束-------------------------------------------------  
 
 
 
 ---------------------------插入盘点单(每个盘点人一张单)---------------------------
 declare cur_create cursor for select distinct created_by from #实盘
 open cur_create
 fetch next from cur_create into @created_by
 while @@FETCH_STATUS = 0
 begin
 
 
 
   exec pro_bill_code '27',@checkorganno,@bill_date,'','','','',@id_no out      
                        
   if @@error <> 0                      
    begin     
		close cur_create
		deallocate cur_create                   
		rollback                      
		raiserror 20010 '产生盘点录入单单号出错'                      
		return                      
    end       
  
  
   
	insert into st_check_m(id_no,checkno,organno,status,remark,created_by,mod_by,created_date,mod_date,billcode,checkdate)
	values(@id_no,@checkno,@organno,0,'盘点机导入',@created_by,@created_by,GETDATE(),GETDATE(),@id_no,null)
	
    if @@error <> 0                      
    begin   
		close cur_create
		deallocate cur_create                     
		rollback                      
		raiserror 20011 '插入盘点录入单单头出错'                      
		return                      
    end       

	insert into st_check_d(id_no,item_no,smatno,batchno,idno,shelfno,lotno,validdate_char,validdate,accqty,
	checkqty,incomtax,incomprice,saleprice,moveprice,miniprice)
	 select @id_no,right('000000' + CONVERT(varchar(10),rowid),6) as item_no,a.smatno,a.batchno,a.idno,a.shelfno,
	 c.lotno,c.validdate_char,c.validdate,b.accqty,a.stockqty,c.incomtax,c.incomprice,c.saleprice,c.moveprice,c.miniprice
	 from #实盘 a,st_check_acc b ,st_stock_l c  
	 where a.smatno=b.smatno and a.batchno=b.batchno and a.idno=b.idno and a.shelfno=b.shelfno  and b.smatno=c.smatno 
	 and b.batchno=c.batchno and b.idno=c.idno and b.checkno=@checkno  and a.created_by = @created_by

    if @@error <> 0                      
    begin 
		close cur_create
		deallocate cur_create                       
		rollback                      
		raiserror 20012 '插入盘点录入单明细出错'                      
		return                      
    end
    
    
    insert into si_sign_states(bill_no,sign_no,item_no,sign_order,ok_flg,duty_desc,waitflg,chkflag,kduser_code)
    values(@id_no,'27',1,5,'N','签核一','Y',0,'*****')
    
    
    if @@error <> 0                      
    begin 
		close cur_create
		deallocate cur_create                       
		rollback                      
		raiserror 20013 '插入签核一出错'                      
		return                      
    end
    
    
    
    insert into si_sign_states(bill_no,sign_no,item_no,sign_order,ok_flg,duty_desc,waitflg,chkflag,kduser_code)
    values(@id_no,'27',2,10,'N','签核二','N',0,'*****')
    
    
    if @@error <> 0                      
    begin 
		close cur_create
		deallocate cur_create                       
		rollback                      
		raiserror 20013 '插入签核二出错'                      
		return                      
    end
    
    
 
	fetch next from cur_create into @created_by
 end
 close cur_create
 deallocate cur_create
 
        
                
commit 
go
if OBJECT_ID('vw_print_price') is not null drop view vw_print_price
go
CREATE view vw_print_price   
as  
SELECT a.smatno,a.smatname,a.smatspec,a.producer,a.smatarea,a.smatunit,a.minunit,  
           a.minnum,a.scanno,a.organno,b.saleprice,b.vipsaleprice,  
       b.minsaleprice,b.cminsaleprice,a.smatcomname,b.memflag,  
       cast('' as varchar(100)) memo,a.created_by,a.usersign,  
       c.para as titletext  
FROM    (SELECT   a.smatno,a.smatname,a.smatspec,a.producer,a.smatarea,a.smatunit,a.minunit,a.minnum,a.scanno,  
                                 b.organno,B.salegroupid,a.smatcomname,a.created_by,a.usersign  
           FROM      ba_smat a left join (SELECT st_stock_m.organno organno,ba_organ.salegroupid salegroupid,st_stock_m.smatno SMATNO 
                                          FROM   st_stock_m left join ba_organ  on st_stock_m.organno = ba_organ.organno ) b on a.smatno  = b.smatno                            
                   ) a  left join  (select  aa.groupid,aa.smatno smatno,  
                                            aa.saleprice saleprice ,bb.saleprice vipsaleprice,  
                                            aa.minsaleprice minsaleprice,bb.minsaleprice cminsaleprice ,  
                                            aa.memflag  
                                     from    ba_smat_saleprice aa left join ba_smat_saleprice_level bb on aa.groupid = bb.groupid and aa.smatno = bb.smatno and ISNULL(bb.viplevel,'01') = '01'  
                                                    ) b on a.smatno = b.smatno and b.groupid = a.salegroupid ,sy_ini c
                                                    where c.ini = '4502'
                                                      
GO

if COL_LENGTH('ls_saleput_d','ybflg') is null
alter table  ls_saleput_d add ybflg int
go




